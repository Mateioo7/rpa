﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.ComponentModel;

namespace IntegerActivities
{
    public class AssignRandomInteger : CodeActivity {
        [Category("Input/Output")]
        public InOutArgument<int> Input { get; set; }

        [Category("Input")]
        public InArgument<int> MinValueInclusive { get; set; }

        [Category("Input")]
        public InArgument<int> MaxValueExclusive { get; set; }

        protected override void Execute(CodeActivityContext context) {
            var inputValue = Input.Get(context);
            var minValueInclusive = MinValueInclusive.Get(context);
            var maxValueExclusive = MaxValueExclusive.Get(context);

            inputValue = new Random().Next(minValueInclusive, maxValueExclusive);
            Input.Set(context, inputValue);

            Console.WriteLine("[Assign Random Integer Activity]: randomly computed " + inputValue + " from range: [" + minValueInclusive + ", " + maxValueExclusive + ")");
        }
    }
}
